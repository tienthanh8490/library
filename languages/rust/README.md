See also:

- [Rust: beyond the typechecker](https://blog.merigoux.ovh/en/2019/04/16/verifying-rust.html) and [rox-star](https://github.com/denismerigoux/rox-star).
- [The Book](https://doc.rust-lang.org/book/)
- [The Rustonomicon](https://doc.rust-lang.org/nomicon/)

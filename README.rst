================
 Library
================

These are papers, textbooks, and articles which I've read and found useful or
interesting.  Inclusion of a text within my personal library does not
constitute condonement, approval, endorsement, or agreement of the claims
and/or opinions expressed within.

Further, I make zero guarantees w.r.t. safety of parsing the files contained
within this repository.  *PARSE AT YOUR OWN RISK!*

With probability negligibly < 1, I will *not take pull requests* for this
repository.  However, reading suggestions are welcome, particularly when they
come with some explanation as to the relevance or worth of the material.

------------------------------------------------
Machine Learning
------------------------------------------------

`UCL Course on RL <http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html>`__
and `Interpretable Machine Learning <https://christophm.github.io/interpretable-ml-book/>`__.

------------------------------------------------
 Data Structures & Algorithms
------------------------------------------------

The best reference for this is Knuth's `The Art of Computer Programming
<https://cs.stanford.edu/~knuth/taocp.html>`__.

------------------------------------------------
 Anonymity, Privacy, & Censorship Circumvention
------------------------------------------------

For further resources in anonymity and privacy, see also `Freehaven's AnonBib
<https://www.freehaven.net/anonbib/>`__.

For resources on online censorship and censorship circumvention, see Philipp
Winter's `CensorBib <https://censorbib.nymity.ch/>`__ (also `on Github
<https://github.com/NullHypothesis/censorbib>`__).

------------------------------------------------
 Quantum Programming Languages
------------------------------------------------

For further resources, see also Professor Simon Gay's `Bibliography on Quantum
Programming Languages <http://www.dcs.gla.ac.uk/~simon/quantum/>`__.

------------------------------------------------
 Exploit Development
------------------------------------------------

For further resources on ARM, see also Azeria Labs `ARM Assembly, Reversing,
and Exploitation tutorials <https://azeria-labs.com/>`__.
